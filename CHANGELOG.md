# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- tfsec custom checks

## [v2.0.0] - 22/04/2020

### added

- tfsec 0.39.20
- enhanced secrets management
- enhanced exceptions management
- access and secret keys rotation

### changed

- upgraded to terraform 0.14.10
- upgraded to aws-vault 6.3.1
- upgraded to chamber 2.9.1

### removed

- terraform 0.13.4 support

## [v1.4.0] - 22/10/2020

### removed

- terraform 0.12.28 support

### added

- init persistance
- init workspace(s) definition

### changed

- upgraded to terraform 0.13.4
- upgraded to aws-vault 6.2.0
- upgraded to chamber 2.8.2

## [v1.3.0] - 29/07/2020

### added

- Windows 10 support @jorge.sanmartin

### fixed

- preview at terraform 0.12.28
- terrax axe path definition @jorge.sanmartin

## [v1.2.0] - 01/07/2020

### added

- terraform 0.12.28 support
- allowed to select between terraform 0.12.0 and 0.12.28

### removed

- terraform 0.12.20 support

## [v1.1.0] - 26/06/2020

### changed

- upgraded to terraform 0.12.20

## [v1.0.0] - 31/03/2020

### changed

- project variable obtained from terraform.tf **(NOT backward compatible)**
- enhanced preview command
- enhanced list command
- README.md

### added

- escaped chamber key value

## [v0.1.0] - 19/03/2020

### added

- moved to awsvault 5.3.2
- moved to chamber 2.8.0
- MFA support
