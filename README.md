# ![Alt text](images/banner.jpg?raw=true "Terrax")
Terrax is a simple orchestration tool that simplify the adoption of aws-vault, chamber and tfsec while terraforming.

Terrax was designed to accomplish terraform community best practices, our main goal is to encrypt secrets used at our terraform templates with minimun cost (1€/Month due to KMS key usage).

Designed at DIA Group (2019) and upgraded at RepsolEyG (2019-2021).

## Pre-installed software required
1) curl
2) unzip

## Certified OS and versions
- Ubuntu (16.04 LTS, 18.04 LTS, 19.10, 20.04 LTS)
- Windows 10 + WSL Ubuntu 20.04 LTS

## Installation
1) chmod +x terrax
2) ./terrax install

## Usage instructions
Terrax has been designed to be based on the terraform comunity best practices and to simply the IaC management at AWS.

1) Configure your aws profile and use [crossaccount roles](https://docs.aws.amazon.com/es_es/IAM/latest/UserGuide/tutorial_cross-account-with-roles.html) at your provider(s) definition(s) to jump between accounts.
2) Define [s3 as terraform backend](https://www.terraform.io/docs/language/settings/backends/s3.html) and use one workspace per product.
3) Use [teraform workspaces](https://www.terraform.io/docs/cli/workspaces/index.html) to manage your different environments (dev, pre and pro and so on).
4) Define your secret(s) variable(s) in capital letters without a value, just use terrax secret add instead (see below example).
5) Enjoy the ride.

### HowTo define a secret
1) select a workspace.
```
terrax workspace select dev
```

2) Define uppercase variables at your terraform templates.
```
# SECRETS
variable "USER" {}
variable "PASS" {}
```

3) Define those secrets and its values. 

```
terrax secret add user nobody
terrax secret add pass nowhere
```

4) Just run a terraform plan and those secrets will be recovered at runtime.
```
terrax plan
```

## Commands list and help
```
terrax v2.0.0 help..........................................
 + check                     -> tfsec check
 |-> check exception         -> tfsec exception list
 |-> check exception add     -> add tfsec exception
 |-> check exception remove  -> remove tfsec exception
............................................................
 + config                    -> awsvault config status
 |-> config profile          -> configure awsvault profile
 |-> config rotate           -> rotate awsvault credentials
 |-> config version          -> configure terraform version
............................................................
 + secret                    -> chamber secrets list
 |-> secret add              -> add chamber secret
 |-> secret remove           -> remove chamber secret
 |-> secret compare          -> chamber secrets comparation
............................................................
 + init                      -> terraform init mock
 + install                   -> terrax install
 + uninstall                 -> terrax uninstall
............................................................
 !!!*whateverelse* will be forwarded to terraform
............................................................
```

## 3rd party software:
- [aws-vault](https://github.com/99designs/aws-vault) 
- [chamber](https://github.com/segmentio/chamber)
- [terraform](https://www.terraform.io/)
- [tfsec](https://tfsec.dev/)

## License
See [LICENSE](LICENSE) file.
Don't blame me if terrax eats your homework.